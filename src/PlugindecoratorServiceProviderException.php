<?php

namespace Drupal\plugindecorator;

/**
 * Exception in PlugindecoratorServiceProvider.
 *
 * We keep the lowercase d used there for tech reasons.
 */
class PlugindecoratorServiceProviderException extends \Exception {

}
